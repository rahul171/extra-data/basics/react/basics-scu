import React from 'react';
import A1 from './more/A1';
import A2 from './more/A2';
import A3 from './more/A3';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            a: 1
        }
    }

    render() {
        return(
            <div className="app">
                <A1 a={this.state.a} />
                <hr />
                <A2 a={this.state.a} />
                <hr />
                <A3 a={this.state.a} />
                <hr />
                <button
                    onClick={() => { this.setState(state => ({ a: this.state.a + 1 })) }}
                >+1 child props => a</button>
                <button
                    onClick={() => { this.setState(state => ({ a: this.state.a })) }}
                >= child props => a</button>
            </div>
        );
    }
}
