import React from 'react';

export default class A1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            a: 1,
            b: 2
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate A1');
    }

    componentDidMount() {
        console.log('componentDidMount A1');
    }

    render() {
        console.log('render A1');

        return(
            <div className="a1">
                <div className="header">A1</div>
                <div>{this.state.a}</div>
                <button
                    onClick={() => { this.setState(state => ({ a: state.a + 1 })) }}
                >+1 a</button>
                <button
                    onClick={() => { this.setState(state => ({ b: state.b + 1 })) }}
                >+1 b</button>
                <button
                    onClick={() => { this.setState(state => ({ a: state.a })) }}
                >= a</button>
                <button
                    onClick={()=>{
                        // see description for this in A3.js
                        this.state.a++;
                    }}
                >change this.state.a</button>
                <button
                    onClick={()=>{this.setState({})}}
                >run change detection</button>
                <button
                    onClick={()=>{}}
                >btn</button>
            </div>
        );
    }
}
