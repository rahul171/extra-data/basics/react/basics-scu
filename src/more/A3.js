import React from 'react';

export default class A3 extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            a: 1,
            b: 2,
            c: { a: 1 },
            d: [1,2,3,4,5]
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate A3');
    }

    componentDidMount() {
        console.log('componentDidMount A3');
    }

    render() {
        console.log('render A3');

        return(
            <div className="a3">
                <div className="header">A3 - PureComponent</div>
                <div>{this.state.a}</div>
                <div>{this.state.d.join(' - ')}</div>
                <button
                    onClick={() => { this.setState(state => ({ a: state.a + 1 })) }}
                >+1 a</button>
                <button
                    onClick={() => { this.setState(state => ({ b: state.b + 1 })) }}
                >+1 b</button>
                <button
                    onClick={() => { this.setState(state => ({ a: state.a })) }}
                >= a</button>
                <button
                    onClick={() => { this.setState(state => ({ c: state.c })) }}
                >= c</button>
                <button
                    onClick={() => { this.setState(state => {
                        console.log('this will trigger component update because c is assigned to a newly created object');
                        console.log(Object.getOwnPropertyDescriptors(state));

                        return {
                            c: { a: 1 }
                        };
                    }) }}
                >=new c</button>
                <button
                    onClick={() => {
                        const d = this.state.d;
                        d.push(d.length + 1);
                        this.setState({ d });
                    }}
                >change d (mutation)</button>
                <button
                    onClick={() => {
                        const d = this.state.d.slice();
                        d.push(d.length + 1);
                        this.setState({ d });
                    }}
                >change d (no mutation)</button>
                <button
                    onClick={()=>{
                        // if you do this in a this.setState callback, doesn't mater if you return 'a' or not,
                        // whatever you return, if the returned value is different, it will call render,
                        // hence, printing the new value of a.
                        // see below button's onClick.
                        this.state.a++;
                    }}
                >change this.state.a</button>
                <button
                    onClick={()=>{
                        this.setState(state => {
                            state.a++;
                            return { b: state.b };
                        });
                    }}
                >change this.state.a in setState with same stats</button>
                <button
                    onClick={()=>{
                        this.setState(state => {
                            state.a++;
                            return { b: state.b + 1 };
                        });
                    }}
                >change this.state.a in setState with new stats</button>
                <button
                    onClick={()=>{this.setState({})}}
                >run change detection</button>
                <button
                    onClick={()=>{console.log(this.state)}}
                >log state</button>
                <button
                    onClick={()=>{}}
                >btn</button>
            </div>
        );
    }
}
