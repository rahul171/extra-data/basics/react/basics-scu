import React from 'react';

export default class A2 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            a: 1,
            b: 2,
            d: [1,2,3,4,5],
            e: { a: 1 }
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate A2');
    }

    componentDidMount() {
        console.log('componentDidMount A2');
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldComponentUpdate A2');

        console.log(this.state);
        // nextState is a new object, but the object in it which aren't changes points to the same memory address
        // as in this.state.
        console.log(nextState);
        // if d changed but with mutation, then this will be true.
        console.log(this.state.d === nextState.d);
        // e isn't changed, and hence its always true.
        // just wanted to show that nextState is a new object, but the objects inside it which aren't changed
        // are the same as in this.state.
        console.log(this.state.e === nextState.e);
        // not the same objects, so always false.
        console.log(this.state === nextState);

        return nextState.a !== this.state.a;
    }

    render() {
        console.log('render A2');

        return(
            <div className="a2">
                <div className="header">A2 - shouldComponentUpdate</div>
                <div>{this.state.a}</div>
                <div>{this.state.d.join(' - ')}</div>
                <button
                    onClick={() => { this.setState(state => ({ a: state.a + 1 })) }}
                >+1 a</button>
                <button
                    onClick={() => { this.setState(state => ({ b: state.b + 1 })) }}
                >+1 b</button>
                <button
                    onClick={() => { this.setState(state => ({ a: state.a })) }}
                >= a</button>
                <button
                    onClick={() => {
                        const d = this.state.d;
                        d.push(d.length + 1);
                        this.setState({ d });
                    }}
                >change d (mutation)</button>
                <button
                    onClick={() => {
                        const d = this.state.d.slice();
                        d.push(d.length + 1);
                        this.setState({ d });
                    }}
                >change d (no mutation)</button>
                <button
                    onClick={()=>{
                        // see description for this in A3.js
                        this.state.a++;
                    }}
                >change this.state.a</button>
                <button
                    onClick={()=>{this.setState({})}}
                >run change detection</button>
                <button
                    onClick={()=>{}}
                >btn</button>
            </div>
        );
    }
}
