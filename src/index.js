import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Keys from './Keys';
import CaseComponents from "./components-name-case";
import EventInCallback from "./event-in-callback";

// import './test';
// import './test2-this';

ReactDOM.render(<EventInCallback />, document.getElementById('root'));
