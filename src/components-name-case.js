import React from 'react';

class lowercaseComponent extends React.Component {
    render() {
        return <div>lowercase</div>
    }
}

class UppercaseComponent extends React.Component {
    render() {
        return <div>uppercase</div>
    }
}

export default class CaseComponents extends React.Component {
    render() {
        return (
            <div>
                <div>check elements in console, see which gets rendered and which doesn't by JSX</div>
                {/*
                    lowercase elements are treated as normal html dom elements, jsx place them in the dom as it is.
                    but uppercase (camelcase) elements are parsed as user defined jsx elements, replacing it with
                    value returned from the render() method.

                    see the note (yellow box) at the end of
                    https://reactjs.org/docs/components-and-props.html#rendering-a-component
                */}
                <hr />
                <lowercaseComponent />
                <UppercaseComponent />
            </div>
        );
    }
}
