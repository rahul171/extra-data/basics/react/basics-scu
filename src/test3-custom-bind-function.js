// run with node.

// this refers to a new object. but inside a function, it points to a global object.
// thats how node works. so ignore below 2 lines.
this.name = 'global';
console.log(this);

const a = function() {
    console.log('hello ' + this.name);
};

a.bind = function(thisValue) {
    return (...args) => {
        return this.apply(thisValue, args);
    }
}

const c = {name: 'fd'}

a();
a.bind(c)();

debugger;
