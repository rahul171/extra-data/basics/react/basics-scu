import React from 'react';
import produce from 'immer';

export default class Keys extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            abcd: this.generateData(7)
        }
    }

    generateData(n) {
        const out = [];
        for (let i = 0; i < n; i++) {
            out.push({
                id: `id-${i}`,
                name: `name-${i}`
            });
        }
        return out;
    }

    render() {
        // how react works is, it compares the new elements with the old elements based on the key.
        // if key matches and the values are different, it re-renders that element.
        // if keys are not found
        //      => not found in list of old elements but found in new list
        //          => add that element
        //      => not found in new list but found in old list
        //          => remove that element from the old list.
        // now, if you use something like, for eg. array index as a key,
        // then,
        // lets say array has 5 elements
        // [
        //  0 => 'hello-0',
        //  1 => 'hello-1',
        //  2 => 'hello-2',
        //  3 => 'hello-3',
        //  4 => 'hello-4',
        // ]
        // now you remove the second element (index = 1), so the new array would be
        // [
        //  0 => 'hello-0',
        //  1 => 'hello-2',
        //  2 => 'hello-3',
        //  3 => 'hello-4',
        // ]
        // so react would see that value of elements with key = 1, 2, 3 has changed.
        // react thinks this because it is using array index as a key for that element.
        // so react would re-render elements 1, 2, and 3.
        // but if you are using a unique key, lets say element id from the database or something,
        /*
            so initially, the array is:
            [
                0 => { id: 'id-0', value: 'hello-0' },
                1 => { id: 'id-1', value: 'hello-1' },
                2 => { id: 'id-2', value: 'hello-2' },
                3 => { id: 'id-3', value: 'hello-3' },
                4 => { id: 'id-4', value: 'hello-4' }
            ]
            now, react is using item.id as its key.
            so if you remove lets say 2nd element (index = 1), then the array would be,
            [
                0 => { id: 'id-0', value: 'hello-0' },
                1 => { id: 'id-2', value: 'hello-2' },
                2 => { id: 'id-3', value: 'hello-3' },
                3 => { id: 'id-4', value: 'hello-4' }
            ]
            so, react would think that the elements with key = 'id-0', 'id-2', 'id-3', 'id-4' are not changed.
            but the new list doesn't have element with key = 'id-1', which was present in the previous list.
            so, react would remove only that element and won't touch other elements.

            run this react app and add/delete elements, monitor both <ul> in the browser elements tab,
            see what is updated in both list when you add/delete an element.
        */
        const list = this.state.abcd.map((item, index) => {
            return (
                <li key={index}>{item.id} - {item.name}</li>
            );
        });

        const list2 = this.state.abcd.map((item, index) => {
            return (
                <li key={item.id}>{item.id} - {item.name}</li>
            );
        });

        return (
            <div>
                <div>
                    <ul>
                        {list}
                    </ul>
                    <ul>
                        {list2}
                    </ul>
                </div>
                <div>
                    <input type="number" name="index" value={this.state.index} onChange={this.handleChange.bind(this)} />
                    <input type="text" name="value" value={this.state.value} onChange={this.handleChange.bind(this)} />
                    <button onClick={this.changeIt.bind(this)}>change</button>
                    <button onClick={this.deleteIt.bind(this)}>delete</button>
                    {/*
                        why .bind() or a wrapper arrow function? => because of the value of 'this' in the callback.
                        see test2-this.js for more info.
                    */}
                    <button onClick={this.addIt.bind(this)}>add</button>
                    <button onClick={() => { this.logState() }}>log state</button>
                </div>
            </div>
        );
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    changeIt() {
        const abcd = produce(this.state.abcd, (abcd) => {
            abcd[parseInt(this.state.index)].name = this.state.value;
        });

        this.setState({ abcd });
    }

    deleteIt() {
        const abcd = produce(this.state.abcd, (abcd) => {
            abcd.splice(parseInt(this.state.index), 1);
        });
        this.setState({ abcd });
    }

    addIt() {
        const name = this.state.value;
        const abcd = produce(this.state.abcd, (abcd) => {
            // abcd.reverse();
            // abcd.push({
            //     id: `${name}-${abcd.length}`,
            //     name
            // });
            // abcd.reverse();
            abcd.splice(
                parseInt(this.state.index),
                0,
                {
                    id: `${name}-${abcd.length}`,
                    name
                }
            );
        });
        this.setState({ abcd });
    }

    logState() {
        console.log(this);
        console.log(this.state);
    }
}
