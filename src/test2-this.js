this.abcd = 11;

const a = {
    a: 11,
    b: () => {
        console.log(this);
    }
}

const b = {
    a: 111,
    b: function() {
        console.log(this);
        console.log('----');
        a.b();
    }
}

// see the last example of
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions#No_separate_this.
// An arrow function does not have its own this. The this value of the enclosing lexical scope is used;
// arrow functions follow the normal variable lookup rules. So while searching for this which is
// not present in the current scope, an arrow function ends up finding the this from its enclosing scope.
const c = {
    a: 1122,
    b: {
        a: 123,
        c: function() {
            console.log(this);
            (() => {
                // this is not found here. so this of c.b.c() is used.
                console.log(this);
            })();
        },
        d: () => {
            // this is not found here. and this of any object is ONLY AVAILABLE IN ITS METHODS.
            // go one step up the lexical env => b is not a method, so can't access 'this' in b, can access only in any method of b.
            // go one step up the lexical env => c is also not a method, so can't access this in c either.
            // go one step up the lexical env => reached the global environment. where this is available.
            console.log(this);
        }
    }
}

// b.b();
c.b.c();
c.b.d();
