import produce from 'immer';
// const { produce } = require('immer');

console.warn('Expand the object to see the live result of an object');
console.warn('If running in node, then run in debug mode with debbuger or breakpoint ' +
    'at the end to see live results of the objects');

const a = {
    a: 'a',
    b: {
        a: 1,
        b: {
            msg: 'hello',
            aa: [1,2,3]
        }
    },
    c: {
        a: 1,
        b: {
            a: 1
        }
    }
};

console.log('a', a);

// clones the whole object. all n levels.
// Also immutable, i.e. can't change property of the final object. because its immutable.
const b = produce(a, (draft) => {
    draft.b.b.msg = 'aa';
});

// unchanged data is structurally shared, i.e. points to the same address in the memory.
console.log(a.c === b.c);
// changed data doesn't point to the same address in the memory, a new copy is created for the changed object.
console.log(a.b === b.b);
console.log(a.b.b === b.b.b);
// points to the same address in the memory, because this object or any underlying object hasn't changed.
console.log(a.b.b.aa === b.b.b.aa);

// can't assign to a read only property. because the whole object is immutable now.
try {
    b.a = 'b';
} catch (e) {
    console.error(e);
}
console.log('b', b);

// doesn't clone. its the same object.
const c = a;
c.b.b.msg = 'c message';
c.a = 'c';
console.log('c', c);

// only clones 1 level.
const d = Object.assign({}, a, {});
d.b.b.msg = 'd message';
d.a = 'd';
console.log('d', d);

// only clones 1 level.
const e = {...a};
e.b.b.msg = 'e message';
e.a = 'e';
console.log('e', e);

// only clones 1 level.
const f = Object.assign({}, a);
f.b.b.msg = 'f message';
f.a = 'f';
console.log('f', f);

const deepClone = require('lodash.clonedeep');

// clones n level deep.
const h = deepClone(a);
h.b.b.msg = 'h message';
h.a = 'h';
console.log('h', h);

// the same checks done for the 'b' object above (cloned using immer) doesn't produce the same results for 'h' object.
// because 'h' is deep cloned using lodash deep clone, which clones the entire object upto n level
// without considering which properties have changed and which haven't.
console.log(a.c === h.c);
console.log(a.b === h.b);
console.log(a.b.b === h.b.b);
console.log(a.b.b.aa === h.b.b.aa);


// enumerable property loop.
const g = {
    a: 1,
    b: 2,
    c: 3
};

// Enumerables are ignored when looping through the object.
Object.defineProperty(g, 'b', {
    enumerable: false
});

console.log('g', g);

for (const prop in g) {
    console.log(prop);
}
