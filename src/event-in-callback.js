import React from 'react';

export default class EventInCallback extends React.Component {

    handleClick = (id, e) => {
        console.log(this);
        console.log(id);
        console.log(e);
    }

    handleClick1 = (e) => {
        console.log(this);
        console.log('works?');
        console.log(e);
    }

    render() {
        return (
            <div>
                {/*
                    once bound, it doesn't matter what you bind, it will always have this fixed.
                    try binding undefined, this will still point to the current class.
                 */}
                <button onClick={this.handleClick.bind(this, 'abcd1')}>click1</button>
                <button onClick={(e) => { this.handleClick('abcd2', e); }}>click2</button>
                <button onClick={this.handleClick1}>clickWithoutAgruments</button>
            </div>
        );
    }
}
